package abstractMethod;

public class Ant extends Bug{
    public Ant (String name){
        this.name = name;
    }

    @Override
    void green() {
        System.out.println("The Ant " + name + " color is green");
    }
}
