package Polymorphism;

public class Guard extends Citizen{

     Guard(String name) {
        super(name);
    }

    @Override
    void sayHello() {
        super.sayHello();
    }
}
