package Logic.logicInterface.logic01Impl;

import Logic.BasicLogic;
import Logic.logicInterface.LogicInterface;

public class Logic01Soal02Impl implements LogicInterface {

    private final BasicLogic logic;

    public Logic01Soal02Impl(BasicLogic logic) {
        this.logic = logic;
    }

    void isiArray(){
        int j = 1;
        int k = 1;
        for (int i = 0; i < this.logic.n; i++) {
            if (i %2 == 0){
                this.logic.array[0][i] = String.valueOf(j);
                j++;

            }else {

                this.logic.array[0][i] = String.valueOf(k*3);
                k++;
            }
        }
    }

    @Override
    public void cetakArray() {
        this.isiArray();
        this.logic.printSingle();
    }
}
