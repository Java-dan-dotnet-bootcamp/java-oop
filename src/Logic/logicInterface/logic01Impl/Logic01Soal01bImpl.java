package Logic.logicInterface.logic01Impl;

import Logic.BasicLogic;
import Logic.logicInterface.LogicInterface;

public class Logic01Soal01bImpl implements LogicInterface {

    private final BasicLogic logic;

    public Logic01Soal01bImpl(BasicLogic logic) {
        this.logic = logic;
    }

    void isiArray(){
        for (int i = 0; i < this.logic.n ; i++) {
            this.logic.array[0][i] = String.valueOf(i+1);

        }
    }

    @Override
    public void cetakArray() {
        this.isiArray();
        this.logic.printSingle();
    }
}
