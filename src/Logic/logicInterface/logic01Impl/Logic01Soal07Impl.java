package Logic.logicInterface.logic01Impl;

import Logic.BasicLogic;
import Logic.logicInterface.LogicInterface;

public class Logic01Soal07Impl implements LogicInterface {

    private final BasicLogic logic;

    public Logic01Soal07Impl(BasicLogic logic) {
        this.logic = logic;
    }

    public void isiArray(){
        char k = 'A';

        for (int i=0; i< this.logic.n; i++) {

            String j =(this.logic.array[0][i] = String.valueOf(k));
            k++;
            this.logic.array[0][i] = j;

        }
    }

    @Override
    public void cetakArray() {
        this.isiArray();
        this.logic.printSingle();
    }
}
