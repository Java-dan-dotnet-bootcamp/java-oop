package Logic.logicInterface.logic01Impl;

import Logic.BasicLogic;
import Logic.logicInterface.LogicInterface;

public class Logic01Soal01aImpl implements LogicInterface {

    private final BasicLogic logic;

    public Logic01Soal01aImpl(BasicLogic logic) {
        this.logic = logic;
    }

    public void isiArray(){
        for (int i = 0; i < this.logic.n; i++) {

            this.logic.array[0][i] = String.valueOf(i+1);

        }
    }

    @Override
    public String toString() {
        return "Logic01Soal01aImpl{" +
                "logic=" + logic +
                '}';
    }

    @Override
    public void cetakArray() {

        this.isiArray();
        this.logic.printSingle();
    }
}
