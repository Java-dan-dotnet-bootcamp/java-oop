package Logic.logicInterface.logic01Impl;

import Logic.BasicLogic;
import Logic.logicInterface.LogicInterface;

public class Logic01Soal09Impl implements LogicInterface {
    private final BasicLogic logic;

    public Logic01Soal09Impl(BasicLogic logic) {
        this.logic = logic;
    }

    public void isiArray(){
        int k = 1;
        for (int i = 0; i < this.logic.n; i++) {
            this.logic.array[0][i] = String.valueOf(k);

            k = Integer.parseInt(this.logic.array[0][i])*3;
        }
    }

    @Override
    public void cetakArray() {
        this.isiArray();
        this.logic.printSingle();
    }
}
