package Logic.Logic01;

import Logic.BasicLogic;

public class Logic01Soal05 extends BasicLogic {


    public Logic01Soal05(int n) {
        super(n);
    }

    public void isiArray(){

        for (int i = 0; i < n; i++){
            if (i < 3) {
                this.array[0][i] = String.valueOf(1);
            } else {
                this.array[0][i] = String.valueOf( Integer.parseInt(this.array[0][i - 1]) + Integer.parseInt(this.array[0][i - 2]) + Integer.parseInt( this.array[0][i-3]));

            }
        }
    }

    public void cetakArray(){
        this.isiArray();
        this.print();
    }
}