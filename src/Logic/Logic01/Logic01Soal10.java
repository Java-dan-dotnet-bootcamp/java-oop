package Logic.Logic01;

import Logic.BasicLogic;

public class Logic01Soal10 extends BasicLogic {

    public Logic01Soal10(int n) {
        super(n);
    }

    public void isiArray() {

        for (int i = 0; i < this.n; i++) {
            double k = (double) (i);
            this.array[0][i] = String.valueOf((int)Math.pow(k, 3));

        }
    }

    public void cetakArray(){
        this.isiArray();
        this.print();
    }
}

