package Logic.Logic02;

import Logic.BasicLogic;

public class Logic02Soal10 extends BasicLogic {
    public Logic02Soal10(int n) {
        super(n);
    }

    public void isiArray(){
    int nilaiTengah = this.n/2;
        for (int i = 0; i < this.n; i++) {
            for (int j = 0; j < this.n; j++) {
                if (j-i >= nilaiTengah || i+j <= nilaiTengah || i-j >= nilaiTengah || i+j >= this.n+3 ){
                    this.array[i][j] = "*";
                }
            }
        }
    }

    public void cetakArray(){
        this.isiArray();
        this.print();
    }
}
