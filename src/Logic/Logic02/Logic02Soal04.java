package Logic.Logic02;

import Logic.BasicLogic;

public class Logic02Soal04 extends BasicLogic {

    public Logic02Soal04(int n) {
        super(n);
    }

    public void isiArray(){
        for (int i = 0; i < this.n; i++) {
            int angka = 1;

            // kolom

            for (int j = 0; j < this.n; j++) {
                if (i == 0) {
                    this.array[i][j] = String.valueOf(angka);
                    angka += 2;
                } else if ( i == this.n - 1 ) {
                    this.array[i][j] = String.valueOf(angka);
                    angka += 2;

                } else if ( j == 0) {
                    this.array[i][j] = String.valueOf(angka);

                } else if (j == this.n - 1) {
                    this.array[i][j] = String.valueOf(17);
                }else if ( i == this.n/2 || j == this.n/2 ) {
                    this.array[i][j] = String.valueOf(0);

                }
            }

        }
    }

    public void cetakArray(){
        this.isiArray();
        this.print();
    }
}
